<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sknf');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '67iw3`T)I`PVlik^U^R)VSGbfSDf?0+])!uU(#doszm-W$fe#@vV2%9B+<Sy.@W4');
define('SECURE_AUTH_KEY',  '0R?bEtj+Huv}i(w`!k`U}30gtALQwogR*QJ.{u6}u0x`:4DO|Tjk*BZtOu;Lx-7l');
define('LOGGED_IN_KEY',    'YbS5CtE@i&w=(:^_jcb3a$xaX%qGq++-JL#{.jlI9_G#UR;TO.0,!Wv7Qd4?68SK');
define('NONCE_KEY',        '|RbM#jt#Jieo*$ukW}<NHN8rMHM_UU3ULG%5y*vG1]n&FQH ^:@yUXJ?zq[a| AE');
define('AUTH_SALT',        'b Aj>2Z+8HN.Btqjf-]1plKPTKA)GMKg<{~L4drEBJxI@.BTg~+zDxe$ Ri;lJYW');
define('SECURE_AUTH_SALT', '}/2KmP<?9:wn[ ZExJ P{2vO<E`vp{&!`W?$5{}j--WJVcf]%|fs:9. jD:Y,#*c');
define('LOGGED_IN_SALT',   'v-:bXy{O(UV+igd,VkwVb500c1B%-n?w]FxvMge9Ad2*qsdSN2C[] {<Mapa#D}&');
define('NONCE_SALT',       'Njpx:Gb2Viow6;}7XxBA!iYx;^o:2*W<pVEI>y*bmKwIk_};TQj(1%Qe/8jgE)i9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sknf_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
