<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shornokishoree
 */

?>

<section class="subscribe">
	<div class="container">
		<div class="row-fluid">
			<div class="col-md-3 wow fadeInLeft" data-wow-duration="0.4s" data-wow-delay="0.4s">
				<h3>subscribe</h3>
			</div>
			<div class="col-md-9">
				<input type="text" placeholder="Enter Your Email Address" />
				<button class="hvr-sweep-to-bottom btn wow fadeInRight btn-transparent btn-submit" data-wow-duration="0.4s" data-wow-delay="0.4s">SUBMIT</button>
			</div>
		</div>
	</div>
</section>

<footer>
	<div class="container">
		<div class="footerContent">
			<div class="row-fluid">
				<div class="col-md-6 logo">
					<div class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Logo" class="pull-left">
						<h2 style="display: inline-block;margin-top: 23px;">Shornokishoree Network Foundation<br/><span>Adolescent Health Network of Bangladesh</span></h2>
					</div>
				</div>
				<div class="col-md-3">
					<h2 class="title">Follow Us</h2>
					<ul class="list-unstyled list-inline social-icon">
						<li class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s"><a href="#."><i class="fa fa-facebook"></i></a></li>
						<li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s"><a href="#."><i class="fa fa-twitter"></i></a></li>
						<li class="wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.9s"><a href="#."><i class="fa fa-linkedin"></i></a></li>
						<li class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s"><a href="#."><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h2 class="title">Quick Links</h2>
				</div>
			</div>
		</div>
		<a id="back-top" href="#top"><i class="fa fa-chevron-up"></i></a>
	</div>
</footer>


<?php wp_footer(); ?>

</body>
</html>
