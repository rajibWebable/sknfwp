<?php get_header(); ?>
   <div class="contact-wrapper">
      <div class="text-center">
         <h2 class="page-title"><?php
             $category = $terms = get_the_terms( $post->ID , 'photoCat' );
             echo $category[0]->name ;?></h2>
      </div>

      <section id="latestPosts">
         <div class="container">
            <div class="row-fluid">
               <?php if(have_posts()) : ?>
               <?php while (have_posts()) : the_post(); ?>
               <div class="col-lg-4">
                  <!-- start page header -->
                  <?php
                     $division_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'division_image' );

                  ?>
                  <a href="<?php the_permalink(); ?>">
                     <div class="photo_thumb">
                        <img src="<?php echo $division_image[0]; ?>" alt="">
                     </div>
                     <h4 class="text-center"><?php the_title(); ?></h4>
                  </a>


                  <!-- end page content -->
               </div>
                  <?php endwhile; ?>
               <?php else : ?>
                  <h3><?php _e('404 Error&#58; Not Found'); ?></h3>
               <?php endif; ?>
            </div>
         </div>
      </section>
   </div>
<?php get_footer(); ?>