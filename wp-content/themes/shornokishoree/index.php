<?php  get_header(); ?>
    <div class="mainContent">
        <section class="slider">
            <div class="youtubeIcon text-center">
                <a class="bla-1" href="https://www.youtube.com/watch?v=UuY3GdFyqqY">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/youtubeIcon.png" alt="youtube Icon" />
                </a>
            </div>
        </section>
        <section class="workTogether">
            <div class="container">
                <div class="col-md-4">
                    <div class="workTogetherLeft">
                        <div id="extraBg"></div>
                        <div class="wow fadeInLeft" data-wow-duration="0.6s" data-wow-delay="0.6s">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/workTogether1.png" alt="cartoon" />
                            <p>We believe that in every community
                                there are girls with  untapped  potential
                                ready to make  our  country
                                a better place.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="workTogetherRight pull-right">
                        <div class="workContent" <?php
                        if ( function_exists( 'ot_get_option' ) ) {
                            $champion_img = ot_get_option( 'champion_img', '' );
                            if($champion_img){
                                echo "style='background-image: url($champion_img);'";
                            }
                        }
                        ?>>
                            <div>
                                <h2 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">We<br/> Work<br/> Together</h2>
                                <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">A girl child who has even a little bit of health education is more conscious of family planning, health care and, in turn, her children’s own education.</p>
                                <button class="hvr-sweep-to-bottom btn btn-default btn-transparent wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">ADOLESCENT REGISTRATION</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
        </section>
        <section class="support">
            <div class="container">
                <div class="col-md-12">
                    <div class="supportContent">
                        <h2 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">Your support<br/>
                            is powerful</h2>
                        <div class="border wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.6s"></div>
                        <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">A girl child who has even a little bit of health education is more conscious of family planning, health care and, in turn, her<br/> children’s own education.</p>
                        <button class="hvr-sweep-to-bottom wow fadeInUp btn btn-default btn-transparent" data-wow-duration="1s" data-wow-delay="1s">Support us</button>
                    </div>
                </div>
            </div>
        </section>
        <section class="features">
            <div class="featuresContent">
                <div class="container">
                    <div class="row-fluid">
                        <div class="col-md-3 features-item">
                            <div class="FeatureDetails text-center">
                                <img class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s" src="<?php echo get_template_directory_uri(); ?>/images/featuresIcon.png" alt="features Icon" />
                                <h3 class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">Sponsor a<br/> child</h3>
                                <p class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">Being loved, wanted and cared for-that is what your sponsorship provides every child that we are blesses to help. 5k tk/month covers food,uniforms, school supplies as well as access to medical care and education costs. Every chils deserve to feel someone cares.</p>
                                <button class="btn btn-donate">Donate Now</button>
                            </div>
                        </div>
                        <div class="col-md-3 features-item">
                            <div class="FeatureDetails text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/featuresIcon.png" alt="features Icon" class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s"/>
                                <h3 class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">Sponsor a<br/> project</h3>
                                <p class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">Being loved, wanted and cared for-that is what your sponsorship provides every child that we are blesses to help. 5k tk/month covers food,uniforms, school supplies as well as access to medical care and education costs. Every chils deserve to feel someone cares.</p>
                                <button class="btn btn-donate">Donate Now</button>
                            </div>
                        </div>
                        <div class="col-md-3 features-item">
                            <div class="FeatureDetails text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/featuresIcon.png" alt="features Icon" class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s"/>
                                <h3 class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">General<br/> donation</h3>
                                <p class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">Being loved, wanted and cared for-that is what your sponsorship provides every child that we are blesses to help. 5k tk/month covers food,uniforms, school supplies as well as access to medical care and education costs. Every chils deserve to feel someone cares.</p>
                                <button class="btn btn-donate">Donate Now</button>
                            </div>
                        </div>
                        <div class="col-md-3 features-item">
                            <div class="FeatureDetails text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/featuresIcon.png" alt="features Icon" class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s" />
                                <h3 class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">Take part<br/> in a tricp</h3>
                                <p class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">Being loved, wanted and cared for-that is what your sponsorship provides every child that we are blesses to help. 5k tk/month covers food,uniforms, school supplies as well as access to medical care and education costs. Every chils deserve to feel someone cares.</p>
                                <button class="btn btn-donate">Donate Now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="joinUs">
            <div class="container">
                <div class="col-md-3">
                    <p class="hidden">A girl child who has even a little bit of health education is more conscious of family planning, health care and, in turn, her children’s own education.</p>
                </div>
                <div class="col-md-9">
                    <div class="JoinContent">
                        <div class="border wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s"></div>
                        <p class="wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">A girl child who has even a little bit of health education is more conscious of family planning, health care and, in turn, her children’s own education.</p>
                        <div class="border wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s"></div>
                        <br/>
                        <br/>
                        <button class="hvr-sweep-to-bottom btn btn-default btn-transparent wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">Join us</button>
                    </div>
                </div>
            </div>
        </section>
        <section class="ProgramList">
            <div class="container">
                <div class="col-md-4 programCol1 noPadding wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">
                    <div class="pull-right">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/tbProgram.jpg" alt="tv Program" class="FocusImg">
                        <div class="box-icon boxIcon1">
                            <a href="about.html#tabTvprogram">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon2.png" alt="icon" />
                                <h3>TV<br/> Programs</h3>
                                <p>Shorno Kishoree Health<br/> education tv programs.</p>
                            </a>
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/img1.jpg" alt="image one" class="FocusImg wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.8s"/>
                        <div class="box-icon boxIcon2 wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.8s">
                            <a href="<?php echo site_url(); ?>/about-us/#activitytab-activity72">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/icon1.png" alt="icon" />
                                <h3>Leadership<br/> Development</h3>
                                <p>Shorno Kishoree Health<br/> education tv programs.</p>
                            </a>
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/img2.jpg" alt="image two" class="FocusImg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s" />
                    </div>
                </div>
                <div class="col-md-4 programCol2 noPadding">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/delaMae.png" alt="tv Program" class="FocusImg wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.9s">
                    <div class="box-icon boxIcon3 wow fadeInUp" data-wow-duration="0.9s" data-wow-delay="0.9s">
                        <a href="about.html#healthClub">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon3.png" alt="icon" />
                            <h3>Health<br/> Club</h3>
                            <p>School Girl Health club</p>
                        </a>
                    </div>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/img3.jpg" alt="Images Three" class="FocusImg wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                    <div class="box-icon boxIcon3 wow fadeInUp" data-wow-duration="2s" data-wow-delay="2s">
                        <a href="#.">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon4.png" alt="icon" />
                            <h3>Equity<br/> & <br/> Justice</h3>
                            <p>Shorno Kishoree Health<br/> education tv programs.</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 programCol3 noPadding">
                    <div class="box-icon boxIcon4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                        <a href="#.">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon5.png" alt="icon" />
                            <h3>Model<br/> Village</h3>
                            <p>Shorno Kishoree Health<br/> education tv programs.</p>
                        </a>
                    </div>
                    <img style="height: 262px" src="<?php echo get_template_directory_uri(); ?>/images/img6.jpg" alt="Image 6" class="FocusImg  wow fadeInUp" data-wow-duration="0.6" data-wow-delay="0.6s">
                    <div class="box-icon boxIcon4 wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">
                        <a href="#.">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon6.png" alt="icon" />
                            <h3>Legal aid<br/> Service</h3>
                            <p>Shorno Kishoree Health<br/> education tv programs.</p>
                        </a>
                    </div>
                    <img style="height: 300px" src="<?php echo get_template_directory_uri(); ?>/images/img7.jpg" alt="Image 7" class="FocusImg wow fadeInUp" data-wow-duration="2s" data-wow-delay="2s">
                </div>
            </div>
        </section>
        <section class="footerImg">
        </section>
    </div>

<?php get_footer(); ?>
