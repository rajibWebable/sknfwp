<?php
/**
 * Initialize the custom Meta Boxes. 
 */
add_action( 'admin_init', 'custom_meta_boxes' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function custom_meta_boxes() {
  
  /**
   * Create a custom meta boxes array that we pass to 
   * the OptionTree Meta Box API Class.
   */
  $data_post_meta_box = array(
    'id'          => 'data_meta',
    'title'       => __( 'Options', 'theme-text-domain' ),
    'desc'        => '',
    'pages'       => array( 'advisory_council' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => __( 'Options', 'theme-text-domain' ),
        'id'          => 'advisor_options',
        'type'        => 'tab'
      ),
      array(
        'label'       => __( 'Position', 'theme-text-domain' ),
        'id'          => 'position',
        'type'        => 'text',
        'desc'        => sprintf( __( 'Enter The Position', 'theme-text-domain' ), '<code>on</code>' ),
        'std'         => ''
      ),
      array(
        'label'       => __( 'Twitter Link', 'theme-text-domain' ),
        'id'          => 'twitter_link',
        'type'        => 'text',
        'desc'        => sprintf( __( 'Enter The Twitter Link', 'theme-text-domain' ), '<code>on</code>' ),
        'std'         => ''
      ),
      array(
        'label'       => __( 'Instragram Link', 'theme-text-domain' ),
        'id'          => 'insta_link',
        'type'        => 'text',
        'desc'        => sprintf( __( 'Enter The Instragram Link', 'theme-text-domain' ), '<code>on</code>' ),
        'std'         => ''
      ),
    )
  );


  /**
   * Create a custom meta boxes array that we pass to
   * the OptionTree Meta Box API Class.
   */
  $photo_meta_box = array(
    'id'          => 'photo_meta',
    'title'       => __( 'Gallery', 'theme-text-domain' ),
    'desc'        => '',
    'pages'       => array( 'division_photos' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'label'       => __( 'Photos', 'theme-text-domain' ),
        'id'          => 'gallery_photos',
        'type'        => 'gallery',
        'desc'        => sprintf( __( 'Enter The Position', 'theme-text-domain' ), '<code>on</code>' ),
        'std'         => ''
      ),
    )
  );

  /**
   * Create a custom meta boxes array that we pass to
   * the OptionTree Meta Box API Class.
   */
  $post_meta_box = array(
    'id'          => 'post_meta',
    'title'       => __( 'Options', 'theme-text-domain' ),
    'desc'        => '',
    'pages'       => array( 'post' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
        array(
            'id'          => 'latest_select',
            'label'       => __( 'Latest', 'theme-text-domain' ),
            'desc'        => __( 'Select Yes or no for your latest post', 'theme-text-domain' ),
            'std'         => '',
            'type'        => 'select',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => '',
            'condition'   => '',
            'operator'    => 'and',
            'choices'     => array(
                array(
                    'value'       => 'latest',
                    'label'       => __( 'Latest', 'theme-text-domain' ),
                    'src'         => ''
                ),
                array(
                    'value'       => 'hot',
                    'label'       => __( 'Hot', 'theme-text-domain' ),
                    'src'         => ''
                ),
                array(
                    'value'       => 'old',
                    'label'       => __( 'old', 'theme-text-domain' ),
                    'src'         => ''
                ),
                array(
                    'value'       => 'no',
                    'label'       => __( 'No Option', 'theme-text-domain' ),
                    'src'         => ''
                ),
            ),
        ),
    )
  );


    

  
  /**
   * Register our meta boxes using the 
   * ot_register_meta_box() function.
   */
  if ( function_exists( 'ot_register_meta_box' ) )
    ot_register_meta_box( $data_post_meta_box );
    ot_register_meta_box( $photo_meta_box );
    ot_register_meta_box( $post_meta_box );

}