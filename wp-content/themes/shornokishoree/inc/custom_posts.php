<?php

/*
****************************************************
****************************************************


========  All Custom Posts For this Theme  ========


****************************************************
****************************************************
*/


function kaps_custom_posts() {
    /* advisory council  */
    register_post_type( 'advisory_council',
        array(
			'menu_icon' => 'dashicons-visibility', // https://developer.wordpress.org/resource/dashicons/ select icon from here
			'labels' => array(
				'name' => __( 'Advisory Councils' ),
				'singular_name' => __( 'Advisory Council' ),
				'add_new_item' => __( 'Add New Advisory Council' )
			),
			'public' => true,
			'supports' => array('title', 'editor', 'thumbnail'),
			'has_archive' => false,
			'rewrite' => array('slug' => 'advisory-council-item'),
        )
    );


    /* photo albums  */
    register_post_type( 'division_photos',
        array(
			'menu_icon' => 'dashicons-images-alt2', // https://developer.wordpress.org/resource/dashicons/ select icon from here
			'labels' => array(
				'name' => __( 'Photo Gallery' ),
				'singular_name' => __( 'Photo Gallery' ),
				'add_new_item' => __( 'Add New Photo Gallery' )
			),
			'public'        => true,
			'supports'      => array('title', 'thumbnail'),
			'has_archive'   => false,
			'rewrite'       => array('slug' => 'photo-gallery-item'),
        )
    );

    /* videos  */
    register_post_type( 'videos',
        array(
			'menu_icon' => 'dashicons-video-alt3', // https://developer.wordpress.org/resource/dashicons/ select icon from here
			'labels' => array(
				'name' => __( 'Video Gallery' ),
				'singular_name' => __( 'Video Gallery' ),
				'add_new_item' => __( 'Add New Video' )
			),
			'public'        => true,
			'supports'      => array('title', 'editor', 'thumbnail'),
			'has_archive'   => false,
			'rewrite'       => array('slug' => 'video-gallery-item'),
        )
    );

    /* videos  */
    register_post_type( 'activity',
        array(
			'menu_icon' => 'dashicons-video-alt3', // https://developer.wordpress.org/resource/dashicons/ select icon from here
			'labels' => array(
				'name' => __( 'Activity' ),
				'singular_name' => __( 'Activity' ),
				'add_new_item' => __( 'Add New Activity' )
			),
			'public'        => true,
			'supports'      => array('title', 'editor', 'thumbnail'),
			'has_archive'   => false,
			'rewrite'       => array('slug' => 'activity-item'),
        )
    );
}
add_action( 'init', 'kaps_custom_posts' );   



?>