<?php

/*
****************************************************
****************************************************


========  All Shortcode For this Theme  ========


****************************************************
****************************************************
*/


/*
**********************************
========  Services  ========
**********************************
*/


function advisory_council($atts){
    extract( shortcode_atts( array(
        'section_title' => '',
	), $atts, 'advisory_council' ) );

    $q = new WP_Query(
        array( 'posts_per_page' => -1, 'post_type'=> 'advisory_council')
        );
    
    $list = '';
    
    while($q->have_posts()) : $q->the_post();
        //get the ID of your post in the loop
        $idd = get_the_ID();
        $advisory_council_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'advisory_council' );
        $position = get_post_meta($idd, 'position', true);
        $twitter_link = get_post_meta($idd, 'twitter_link', true);
        $insta_link = get_post_meta($idd, 'insta_link', true);


        $list .= '
       <div class="col-lg-3">
            <div class="single_advisor fix">
                <!-- Button trigger modal -->
                    <div class="single_image">
                        <img src="'.$advisory_council_img[0].'" alt="">
                    </div>
                    <div class="advisortitle">
                        <h4><a href="#" data-toggle="modal" data-target="#myModal'.$idd.'">'.get_the_title().'</a></h4>
                        <p>'.$position.'</p>
                    </div>
                    
                    <!-- Modal -->
                    <div class="modal fade" id="myModal'.$idd.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog  modal-lg" role="document">
                        <div class="modal-content">
                          <div class="modal-body text-center">
                            <img class="img-circle" src="'.$advisory_council_img[0].'" alt="">
                            <h3 class="text-center">'.get_the_title().'</h3>
                            <p class="text-left">'.get_the_content().'</p>
                            
                            <div class="social_icon">
                                <a href="'.$twitter_link.'"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="'.$insta_link.'"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
            </div>
            <!--  end of single service -->
        </div>
        <!--  end of col-lg-4 -->';  
    
    endwhile;
    $list.= '';
    
    wp_reset_query();
    return $list;
}
add_shortcode('advisory_council', 'advisory_council');




function divisions($atts, $content = null) {
    $a = shortcode_atts( array(), $atts );

    $terms = get_terms( 'photoCat', array(
        'hide_empty' => false,
    ) );

    $list = '<h4>Photo Gallery</h4><ul>';

    foreach($terms as $term){
        $list .= '<li><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
    }
    $list .= '</ul>';

    $terms = get_terms( 'videoCat', array(
        'hide_empty' => false,
    ) );

    $list .= '<h4>Video Gallery</h4><ul>';

    foreach($terms as $term){
        $list .= '<li><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
    }
    $list .= '</ul>';

    return $list;
}
add_shortcode("divisions", "divisions");






?>