<?php

/**
 * Enqueue scripts and styles.
 */
function shornokishoree_scripts() {
	wp_enqueue_style( 'gFonts-Montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700', array(), '1.0', 'all');
	wp_enqueue_style( 'gFonts-Roboto', 'https://fonts.googleapis.com/css?family=Roboto:400,500,700', array(), '1.0', 'all');
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.7', 'all');
	wp_enqueue_style( 'fontAwesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0', 'all');
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css', array(), '4.7.0', 'all');
	wp_enqueue_style( 'YouTubePopUp', get_template_directory_uri() . '/vendor/YouTubePopUp/YouTubePopUp.css', array(), '1.0.2', 'all');
	wp_enqueue_style( 'stylesCss', get_template_directory_uri() . '/css/styles.css', array(), '1.0', 'all');
	wp_enqueue_style( 'Shornokishoree', get_stylesheet_uri() );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css', array(), '1.0', 'all');

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true );
	wp_enqueue_script( 'YouTubePopUp', get_template_directory_uri() . '/js/YouTubePopUp.jquery.js', array('jquery'), '1.0.2', true );
	wp_enqueue_script( 'wowJs', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '1.3.0', true );
	wp_enqueue_script( 'scrollJs', get_template_directory_uri() . '/js/scroll.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'mainJS', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0', 'true' );

	
}
add_action( 'wp_enqueue_scripts', 'shornokishoree_scripts' );




?>