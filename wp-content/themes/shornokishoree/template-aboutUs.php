<?php
/*
 Template Name: About Us
 */
get_header(); ?>
    <div class="mainContent">
        <section class="Aboutslider">
            <div class="youtubeIcon text-center">
            </div>
        </section>
        <section class="about-us">
            <div class="what-we-do wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.6s">
                <div class="container">
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <h3 class="blue-title text-left">Who Are We</h3>
                            <p class="text-left">Shornokishore Network Foundation (named as Shornokishoree) is a registered non-profit and social change maker development organization in Bangladesh, started its journey in 2012. Initially we have focused to work with adolescent girls -Shornokishoree (Golden Girl). Later on we also worked with adolescent boys-Surjo Kishor (Empowered Sunny Boy). SKNFgradually expanded its strategic priorities and coverage throughout the country. Our creative strategies to establish secondary school based clubs, health education and classroom based training, peer education at community for non-school going adolescents, community mobilization for addressing barriers, advocacy for supporting policy, programs and utilization of ICT and TV media for reaching the millions of target population for achieving national plan and the SDGs.
                            </p>
                            <p class="text-left">SKNF has started with few districts in 8 divisions and gradually expanded its coverage to 491 upaliza of all 64 districts of Bangladesh. It has orchestrated its reputation as one of the few development & change maker organization connecting from root at community to different levels of society and highest policy level for addressing the adolescent health, development & empowerment issues in BangladeshSKNF has started with few districts in 8 divisions and gradually expanded its coverage to 491 upaliza of all 64 districts of Bangladesh. It has orchestrated its reputation as one of the few development & change maker organization connecting from root at community to different levels of society and highest policy level for addressing the adolescent health, development & empowerment issues in Bangladesh
                            </p>
                        </div>
                        <div class="col-md-12" style="margin-top: 50px">
                            <div class="col-md-6">
                                <h3 class="blue-title text-left">Vision</h3>
                                <p class="text-left">To promote health, rights& development of the adolescent population of Bangladesh, thereby each and every adolescent will get the opportunity for healthy, responsible and independent adults.
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h3 class="blue-title text-left">Mission</h3>
                                <p class="text-left">SKNF exists to promote comprehensive adolescent health, nutrition and development in Bangladesh through providing accurate information, improvement of knowledge& practices, research, community mobilization, advocacy and partnership for sustainable impact.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="about-tv-program">
                <div class="container">
                    <div class="row-fluid">
                        <div class="col-md-6">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/about/img1.jpg" alt="" class="for-tab-img" />
                        </div>
                        <div class="col-md-6 wow fadeInRight" data-wow-duration="0.6s" data-wow-delay="0.6s">
    
                            <img src="<?php echo get_template_directory_uri(); ?>/images/about/icon1.png" />
                            <h3 class="blue-title text-left">Shornokishoree health education TV Programs</h3>
                            <p class="text-left">To make a comprehensive change in this sector, there is no alternative way without creating awareness among the girls and educating them as they are the future mother, so started its activities in 2012 which is a registered non-profit, non-political and social development organization. To make a comprehensive change in this sector, there is no alternative way without creating awareness among the girls and educating them as they are the future mother, so started its activities in 2012 which is a registered non-profit, non-political and social development organization. To make a comprehensive change in this sector, there is no alternative way without creating awareness among the girls and educating them as they are the future mother, so started its activities in 2012 which is a registered non-profit, non-political and social development organization. To make a comprehensive change in this sector, there is no alternative way without creating awareness among the girls and educating them as they are the future mother, so started its activities in 2012 which is a registered non-profit, non-political and social development organization.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="education-tv-program">
                <div class="container">
                    <div class="row-fluid">
                        <div class="col-md-5">
                            <div class="education-tv-program-content wow fadeInLeft" data-wow-duration="0.6s" data-wow-delay="0.6s">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/about/icon2.png" />
                                <h3 class="blue-title text-left">Adolescent Girls’ Health Services Network</h3>
                                <p class="text-left">To make a comprehensive change in this sector, there is no alternative way without creating awareness among the girls and educating them as they are the future mother, so started its activities<br/> in 2012 which is a registered non-profit, non-political and social development organization.<br/> To make a comprehensive change in this sector, there is no alternative way without creating awareness among the girls and educating them as they are the future <br/> mother, so started its activities in 2012 which is a registered non-profit, non-political and social development organization.<br/> To make a comprehensive change in this sector, there is no alternative way without creating awareness among the girls and educating them as they are the future mother, so started its activities in 2012 which is a<br/>registered non-profit, non-political and social development organization.<br/> To make a comprehensive change in this sector, there is no alternative way without creating awareness among the girls and educating them as they are the future mother, so started its activities in 2012 which is a <br/>registered non-profit, non-political and social development organization.</p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/about/img2.jpg" alt="" class="for-tab-img" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="life-style">
            <div class="life-style-content">
                <div class="container">
                    <div class="text-center life-style-content text-center wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.6s">
                        <h2 class="yellow-title">Healthy Lifestyle</h2>
                        <br/>
                        <p>SKNF’s Human Rights and Legal Aid Services programme is dedicated to<br/> protecting and promoting human rights of the women and girls through<br/> legal empowerment.<br/> Our work is premised on a rights based approach to human development.<br/> We have a network based communication with all the 64 District<br/> Commissioners (DC), Police Super (SP) and Civil Surgeon of Bangladesh. </p>
                        <br/>
                        <br/>
                        <button class="btn btn-default blueBtn">Learn More</button>
                    </div>
                </div>
            </div>
        </section>
        <section class="activites" id="activitytab">
            <div class="container">
                <div class="row-fluid">
                    <div class="col-md-6 wow fadeInLeft" data-wow-duration="0.6s" data-wow-delay="0.6s">
                        <div class="activites-header text-right">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/about/icon3.png" alt="" />
                            <h2 class="text-right blue-title">Our<br/> Activities
                            </h2>
                        </div>

                        <div class="activities-list">
                            <ul class="text-right list-unstyled" role="tablist">
                                <?php
                                global $post;
                                $args = array( 'posts_per_page' => 10, 'post_type'=> 'activity', 'order' => 'asc');
                                $myposts = get_posts( $args );
                                if (!empty($myposts)) :
                                    foreach( $myposts as $post ) : setup_postdata($post); ?>

                                    <li role="presentation" class="activity<?php echo get_the_ID(); ?>"><a href="#activity<?php echo get_the_ID(); ?>" aria-controls="activity<?php echo get_the_ID(); ?>" role="tab" data-toggle="tab" class="active"><?php the_title(); ?></a></li>

                                    <?php endforeach; ?>
                                <?php else : ?>
                                    No Data
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="tab-content">
                            <?php
                            global $post;
                            $args = array( 'posts_per_page' => 10, 'post_type'=> 'activity', 'order' => 'asc');
                            $myposts = get_posts( $args );
                            if (!empty($myposts)) :
                                foreach( $myposts as $post ) : setup_postdata($post); ?>

                                    <div role="tabpanel" class="tab-pane fade in activity<?php echo get_the_ID(); ?>" id="activity<?php echo get_the_ID(); ?>">
                                        <div class="tab-content-img">
                                            <?php
                                                $activity_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'activity_image' );
                                            ?>
                                            <img src="<?php echo $activity_image[0]; ?>" class="img-responsive" />
                                        </div>
                                        <div class="tab-title">
                                            <h3 class="blue-title"><?php the_title(); ?></h3>
                                        </div>
                                        <div class="tab-para">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>

                                <?php endforeach; ?>
                            <?php else : ?>
                                No Data
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 150px;"></div>
            <div class="partner-logo" id="partner-logo">
                <div class="container">
                    <h3 class="blue-title text-center" style="margin-bottom: 35px;">Partners & Collaborator’s</h3>
                   <?php echo do_shortcode('[wpaft_logo_slider]'); ?>
                </div>
            </div>

        </section>

        <section class="footerImgQAbout"></section>
    </div>

<?php get_footer(); ?>
