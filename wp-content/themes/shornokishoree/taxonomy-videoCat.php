<?php get_header(); ?>
   <div class="contact-wrapper">
      <div class="text-center">
         <h2 class="page-title"><?php
             $category = $terms = get_the_terms( $post->ID , 'videoCat' );
             echo $category[0]->name ;?></h2>
      </div>

      <section id="latestPosts">
         <div class="container">
            <div class="row-fluid">
               <?php if(have_posts()) : ?>
               <?php while (have_posts()) : the_post(); ?>
               <div class="col-lg-4">
                  <!-- start page header -->

                     <div class="video_thumb">
                         <div class="embed-responsive embed-responsive-4by3">
                             <?php the_content();?>
                         </div>
                     </div>
                     <h4 class="text-center"><?php the_title(); ?></h4>

                  <!-- end page content -->
               </div>
                  <?php endwhile; ?>
               <?php else : ?>
                  <h3><?php _e('No Videos To show'); ?></h3>
               <?php endif; ?>
            </div>
         </div>
      </section>
   </div>

<?php get_footer(); ?>