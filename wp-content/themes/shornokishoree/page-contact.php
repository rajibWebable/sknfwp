<?php
/**
 *
 * Template Name: contact us
 *
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>


    <div class="contact-wrapper">
        <div class="text-center">
            <h2 class="page-title"><?php the_title(); ?></h2>
        </div>
        <?php
        while ( have_posts() ) : the_post();

          get_template_part( 'template-parts/page/content', 'page' );

          // If comments are open or we have at least one comment, load up the comment template.
          if ( comments_open() || get_comments_number() ) :
            comments_template();
          endif;

        endwhile; // End of the loop.
        ?>
    </div>


<?php get_footer(); ?>
