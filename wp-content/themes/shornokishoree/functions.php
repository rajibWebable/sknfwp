<?php
/**
 * Shornokishoree functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Shornokishoree
 */

if ( ! function_exists( 'shornokishoree_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function shornokishoree_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */

	}
endif;
add_action( 'after_setup_theme', 'shornokishoree_setup' );


    /*
    |-------------------------------------------------------------
    |  Required Files
    |-------------------------------------------------------------
    */

    require_once( 'inc/style_scripts.php' );
    require_once('inc/custom_posts.php');
    require_once('inc/wp_bootstrap_navwalker.php');
    require_once( 'inc/shortcodes.php' );

    add_theme_support( 'post-thumbnails', array( 'post', 'advisory_council', 'division_photos', 'videos', 'activity' ) );
    set_post_thumbnail_size( 370, 320, true );
    add_image_size( 'advisory_council', 205, 195, true );
    add_image_size( 'division_image', 320, 190, true );
    add_image_size( 'activity_image', 485, 255, true );


    /*
    |-------------------------------------------------------------
    |  nav menu
    |-------------------------------------------------------------
    */

    add_action('init', 'shornokishoree_register_menu');
    function shornokishoree_register_menu() {
        if (function_exists('register_nav_menu')) {
            register_nav_menu( 'main-menu', __( 'Main Menu', 'shornokishoree' ) );
        }
    }


    /*
    |-------------------------------------------------------------
    |  option tree integration
    |-------------------------------------------------------------
    */


    add_filter( 'ot_show_pages', '__return_false' );
    add_filter( 'ot_show_new_layout', '__return_false' );
    add_filter( 'ot_theme_mode', '__return_true' );
    require_once( 'option-tree/ot-loader.php' );
    require_once( 'inc/ot_menu_function.php' );
    require_once( 'inc/theme-options.php' );
    include_once( 'inc/theme-options_backup.php' );
    require_once( 'inc/meta-boxes.php' );



    /*
    |-------------------------------------------------------------
    |   category for photo album
    |-------------------------------------------------------------
    */


    function photo_category() {
        // Add new "Locations" taxonomy to Posts
        register_taxonomy('photoCat', 'division_photos', array(
            // Hierarchical taxonomy (like categories)
            'hierarchical' => true,
            // This array of options controls the labels displayed in the WordPress Admin UI
            'labels' => array(
                'name' => _x( 'Photo Categories', 'taxonomy general name' ),
                'singular_name' => _x( 'Category', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search Category' ),
                'all_items' => __( 'All Category' ),
                'parent_item' => __( 'Parent Category' ),
                'parent_item_colon' => __( 'Parent Category:' ),
                'edit_item' => __( 'Edit Category' ),
                'update_item' => __( 'Update Category' ),
                'add_new_item' => __( 'Add New Category' ),
                'new_item_name' => __( 'New Category Name' ),
                'menu_name' => __( 'Photo Categories' ),
            ),
            // Control the slugs used for this taxonomy
            'rewrite' => array(
                'slug' => 'photos', // This controls the base slug that will display before each term
                'with_front' => false, // Don't display the category base before "/locations/"
                'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
            ),
        ));
    }
    add_action( 'init', 'photo_category', 0 );

    /*
    |-------------------------------------------------------------
    |   category for videos
    |-------------------------------------------------------------
    */


    function video_category() {
        // Add new "Locations" taxonomy to Posts
        register_taxonomy('videoCat', 'videos', array(
            // Hierarchical taxonomy (like categories)
            'hierarchical' => true,
            // This array of options controls the labels displayed in the WordPress Admin UI
            'labels' => array(
                'name' => _x( 'Video Categories', 'taxonomy general name' ),
                'singular_name' => _x( 'Category', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search Category' ),
                'all_items' => __( 'All Category' ),
                'parent_item' => __( 'Parent Category' ),
                'parent_item_colon' => __( 'Parent Category:' ),
                'edit_item' => __( 'Edit Category' ),
                'update_item' => __( 'Update Category' ),
                'add_new_item' => __( 'Add New Category' ),
                'new_item_name' => __( 'New Category Name' ),
                'menu_name' => __( 'Video Categories' ),
            ),
            // Control the slugs used for this taxonomy
            'rewrite' => array(
                'slug' => 'videos', // This controls the base slug that will display before each term
                'with_front' => false, // Don't display the category base before "/locations/"
                'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
            ),
        ));
    }
    add_action( 'init', 'video_category', 0 );




    /*
    |----------------------------------------------------------
    | remove wp version
    |----------------------------------------------------------
    */

    function remove_version_info() {
        return '';
    }
    add_filter('the_generator', 'remove_version_info');

    // hide admin bar
    show_admin_bar(false);

