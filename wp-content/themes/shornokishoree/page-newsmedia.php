<?php
/**
 *
 * Template Name: News and Media
 *
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>


    <div class="page-wrapper">
      <div class="text-center title-box">
        <h2 class="page-title"><?php the_title(); ?></h2>
        <div class="title-border"></div>
      </div>


        <section id="latestPosts">
            <div class="container">
                <div class="row-fluid">

                    <?php
                    $args = array(
                        'category_name'     => 'press',
                        'posts_per_page'    => -1,
                        'order'             => 'ASC'
                    );
                    $press = new WP_Query( $args );
                    if ( $press->have_posts() ) :
                        while ( $press->have_posts() ) : $press->the_post();
                            ?>

                            <div class="col-sm-4 blog latest-blog">

                                <?php $latest_select = get_post_meta($post->ID, 'latest_select', true);
                                if($latest_select == 'latest'){
                                    echo "<p>Latest</p>";
                                }elseif($latest_select == 'hot'){
                                    echo "<p>Hot</p>";
                                }elseif($latest_select == 'old'){
                                    echo "<p>Old</p>";
                                }

                                ?>
                                <div class="m0 blogInner">
                                    <div class="post_date">
                                       <span class="arrow_box"><i class="fa fa-calendar"></i></span> <span class="date"><?php echo get_the_date();?></span>
                                    </div>
                                    <div class="m0 featureImg">
                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="row m0 postExcerpts">
                                        <div class="m0 postExcerptInner">
                                            <a href="<?php echo get_the_permalink(); ?>" class="postTitle row m0"><h4><?php the_title(); ?></h4></a>
                                            <p><?php echo wp_trim_words( get_the_content(), 26, '' ) ?></p>
                                            <a href="<?php echo get_the_permalink(); ?>" class="readMore">read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php
                        endwhile;
                    endif;
                    wp_reset_query();
                    ?>

                </div>
            </div>
        </section>

    </div>

<?php get_footer(); ?>
