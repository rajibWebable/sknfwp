<?php get_header(); ?>

<div class="page-wrapper">
	<div class="text-center title-box">
		<h2 class="page-title"><?php the_title(); ?></h2>
		<div class="title-border"></div>
	</div>

	<section id="main-content">
		<div class="container">
			<div class="row-fluid">
				<div class="col-lg-12">
					<!-- start page header -->
					<?php if(have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>

							<?php the_content(); ?>

						<?php endwhile; ?>
					<?php else : ?>
						<h3><?php _e('404 Error&#58; Not Found'); ?></h3>
					<?php endif; ?>
					<!-- end page content -->
				</div>
			</div>
		</div>
	</section>

</div>

<?php get_footer(); ?>
