<!DOCTYPE html>
<html class="no-js" lang="">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo();?></title>
	<?php wp_head(); ?>
</head>
<body>
<header>
	<div class="headerContent">
		<div class="row-fluid">
			<div class="col-md-6 logoCol">
				<div class="logo">
					<a href="<?php echo home_url(); ?>/">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo" />
						<h2 style="display: inline-block;margin-top: 7px;">Shornokishoree Network Foundation<br/><span>Adolescent Health Network of Bangladesh</span></h2>
					</a>
				</div>
			</div>
			<div class="col-md-6 menuCol">
				<div class="menuContent pull-right">
					<div class="col-md-3 buttonCol">
						<button class="btn btn-md btn-header"><a href="<?php echo get_permalink(get_page_by_path('donate')); ?>">DONATE</a></button>
					</div>
					<div class="col-md-9">
						<div class="menu">
							<nav>
								<button class="btn" type="button" id="menuButton">
									<img src="<?php echo get_template_directory_uri(); ?>/images/menuIcon.png" alt="menu Icon" id="menuImg" />
								</button>
								<div class="dropdownMenu">
<!--									<ul class="list-unstyled">-->
<!--										<li><a href="about.html">About</a></li>-->
<!--										<li><a href="#.">WHO WE ARE</a></li>-->
<!--										<li><a href="#.">WHAT WE DO</a></li>-->
<!--										<li><a href="#.">WHERE WE WORK</a></li>-->
<!--										<li><a href="#.">MANAGEMENT</a></li>-->
<!--										<li><a href="#.">GALLERY</a></li>-->
<!--										<li><a href="#.">NEWS & EVENTS</a></li>-->
<!--									</ul>-->
									<?php
									if (function_exists('wp_nav_menu')) {
										wp_nav_menu(array(
											'theme_location' => 'main-menu',
											'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
											'walker'         => new wp_bootstrap_navwalker()

										));
									}
									?>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
