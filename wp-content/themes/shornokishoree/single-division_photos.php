<?php
/**

 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>


    <div class="contact-wrapper">
        <div class="text-center">
            <h2 class="page-title"><?php the_title(); ?></h2>
        </div>


        <section id="latestPosts">
            <div class="container">
                <div class="row-fluid">

                <?php
                    global $post;
                    $args = array( 'posts_per_page' => -1, 'post_type'=> 'division_photos');
                    $myposts = get_posts( $args );
                    foreach( $myposts as $post ) : setup_postdata($post); ?>

                            <?php
                            if ( function_exists( 'ot_get_option' ) ) {
                                $images = explode( ',', get_post_meta( get_the_ID(), 'gallery_photos', true ) );
                                if ( ! empty( $images ) ) {
                                    foreach( $images as $id ) {
                                        if ( ! empty( $id ) ) {
                                            $thumb_img_src = wp_get_attachment_image_src( $id, 'gallery-thumb' );
                                            echo '<div class="col-lg-4"><img src="'.$thumb_img_src[0].'" alt="" class="img-responsive"/></div>';
                                        }
                                    }
                                }
                            }
                            ?>


                <?php endforeach; ?>

                </div>
            </div>
        </section>

    </div>

<?php get_footer(); ?>