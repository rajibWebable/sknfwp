/**
 * Created by Akib on 11/1/2017.
 */

jQuery(document).ready(function($){
   $(".dropdownMenu").hide();
    $("#menuButton").click(function () {
        $(".dropdownMenu").toggle('fast');
    });



    /*Initialize POP up video*/
    $(function(){
        $("a.bla-1").YouTubePopUp();
    });

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        //console.log(scroll);
        if (scroll >= 70) {
            //console.log('a');
            $("header").addClass("change");
            $(".dropdownMenu").hide();
            $(".youtubeIcon").hide();
        } else {
            //console.log('a');
            $("header").removeClass("change");
            $(".youtubeIcon").show();
        }
    });




    // hide #back-top first
    $("#back-top").hide();


    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 500) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();

            }
        });

        // scroll body to 0px on click
        $('a#back-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });


    // tab content
    $("#healthClubTab").hide();
    $("#LeadershipTab").hide();
    $("#healthModelTab").hide();
    $("#genderJusticeTab").hide();
    $("#legalServiceTab").hide();
    $("#MobileServiceTab").hide();
    $("#HealthServiceTab").hide();
    $("#ResearchTab").hide();
    $("#approachTab").hide();

    $(document.body).on("click","#healthClub",function(){
      $("#tabTV").hide();
      $("#healthClubTab").show();
    });

    $(document.body).on("click","#leadeShipDevelopment",function(){
      $("#LeadershipTab").show();
      $("#healthClub").hide();
    });


    $('.activities-list ul li:first-child, .activites .tab-content .tab-pane:first-child').addClass('active');

    var url = window.location.hash;
    var url = url.split("-");

    if(url[0] == '#activitytab'){
        $('html,body').animate({scrollTop: $("#activitytab").offset().top}, 'slow');
        $('.activities-list ul li, .activites .tab-content .tab-pane').removeClass('active');
        $('.'+url[1]).addClass('active');
    }




});

new WOW().init();



