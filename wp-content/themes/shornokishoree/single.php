<?php get_header(); ?>


<div class="page-wrapper">
	<div class="text-center title-box">
		<h2 class="page-title"><?php the_title(); ?></h2>
		<div class="title-border"></div>
	</div>


	<section id="latestPosts">
		<div class="container">
			<div class="row-fluid">
				<?php if(have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>

						<?php echo the_content(); ?>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

</div>

<style>
body {
    background-color: #f5f5f5;
}
p{
	color: #000;
	line-height: 27px;
	font-size: 15px;
}
#latestPosts img{
	    margin: 20px 0;
}
</style>
<?php get_footer(); ?>
